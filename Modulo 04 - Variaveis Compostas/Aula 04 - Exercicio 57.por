programa
{
	inclua biblioteca Util --> u
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 057 - Sorteio Invertido }\n\n")

		//Decalração das Variáveis
		inteiro vet[10]

		//Saida de Dados
		escreva("Vou sortear 10 valores...\n")

		//Peenche o Vetor
		para(inteiro i = 0; i < u.numero_elementos(vet); i++)
		{
			vet[i] = sorteia(1,10)
			escreva(i, ":{", vet[i], "} ")
			u.aguarde(500)
		}

		escreva("\nMostrando a sequência invertida...\n")
		
		para (inteiro i = 10; i > u.numero_elementos(vet); i--)
		{
			escreva(i, ":{", vet[i], "} ")
			u.aguarde(500)
		}
		escreva("")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 577; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = {vet, 10, 10, 3};
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */