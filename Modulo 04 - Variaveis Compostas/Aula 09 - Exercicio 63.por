programa
{
	inclua biblioteca Util --> u
	inclua biblioteca Tipos --> t
	inclua biblioteca Matematica --> mat
	
	funcao inicio()
	{
		escreva("{ EXERCICIO 063 - Pessoas e Idades }\n\n")

		cadeia nome[6]
		inteiro idade[6]
		para(inteiro p = 0; p < u.numero_elementos(nome); p++)
		{
			escreva("Nome da pessoa [" + p + "]: ")
			leia(nome[p])
			escreva("Idade de " + nome[p] + ": ")
			leia(idade[p])
		}
		escreva("\n===== ANALISANDO AS PESSOAS CADASTRADAS ======")
		u.aguarde(200)
		
		inteiro soma = 0
		real media = 0
		para(inteiro p = 0; p < u.numero_elementos(nome); p++)
		{
			soma = soma + idade[p]
		}
		media = t.inteiro_para_real(soma) / 6
		escreva("\nMédia de idade: " + mat.arredondar(media, 1) + " anos.")
		escreva("\nPessoas acima da média: ")
		para(inteiro p = 0; p < u.numero_elementos(nome); p++)
		{
			se(idade[p] > media)
			{
				escreva("\n\t-> " + nome[p] + " (" + idade[p] + " anos)")	
			}
		}
		escreva("\n--------------------------------")

		inteiro maior = 0
		cadeia pessoa = " "
		escreva("\nMaior Idade do grupo: ")
		para(inteiro p = 0; p < u.numero_elementos(nome); p++)
		{
			se(p == 0)
			{
				maior = idade[p]
				pessoa = nome[p]
			}
			senao
			{
				se(idade[p] > maior)
				{
					maior = idade[p]
					pessoa = nome[p]
				}	
			}
		}
		escreva(maior + " anos")
		escreva("\nPessoa(s) com a maior idade: ")
		escreva("\n\t-> " + pessoa)
		

		escreva("\n")
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1390; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */