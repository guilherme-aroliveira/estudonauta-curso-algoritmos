programa
{
	inclua biblioteca Util --> u
	
	funcao inicio()
	{
		inteiro  vet[9]

		//Preenchimento de vetor
		para(inteiro p = 0; p < u.numero_elementos(vet); p++)
		{
			vet[p] = sorteia(1, 10)
		}

		escreva("Os valores sorteados são: ")

		//Exibição do Vetor
		para(inteiro p = 0; p < u.numero_elementos(vet); p++ )
		{
			escreva(vet[p], " --> ")
			u.aguarde(400)	
		}
		escreva("FIM\n")

		//Busca pela Chave
		inteiro chave, tot = 0
		escreva("Esta procurano por qual valor? ") 
		leia(chave)
		escreva("Procurando pela chave ", chave, "...\n")
		u.aguarde(1000)
		para(inteiro p = 0; p < u.numero_elementos(vet); p++)
		{
			se(vet[p] == chave)
			{
				escreva("Achado na posição " + p + "\n")
				u.aguarde(400)
				tot++
			}
		}

		//Resultado Final
		se(tot == 0)
		{
			escreva("O valor " + chave + " não foi encontrado dentro do vetor.\n")
		}
		senao
		{
			escreva("O valor " + chave + " foi encontrado " + tot + " vezes dentro do vetor.\n")	
		}
		
		escreva("")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 149; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */