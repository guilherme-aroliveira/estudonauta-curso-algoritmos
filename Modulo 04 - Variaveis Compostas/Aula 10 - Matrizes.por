programa
{
	 
	funcao inicio()
	{
		inteiro x // Variável Simples
		inteiro y[5] // Variável composta (VETOR)
		inteiro z[3][2] // Variável composta (MATRIZ)

		x = 8
		y[3] = 7
		z[2][1] = 6
		z[1][0] = 4

		escreva("FIM")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 218; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = {x, 6, 10, 1}-{y, 7, 10, 1}-{z, 8, 10, 1};
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */