programa
{
	inclua biblioteca Util --> u
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 058 - Fibonacci no Vetor }\n\n")

		//Declaração das Variaveis
		inteiro fib[15]
		inteiro n1 = 0, n2 = 1, soma = 0

		//Prrenche o Vetor
		para(inteiro i = 0; i < u.numero_elementos(fib); i++)
		{
			fib[i] = soma
			soma = n1 + n2
			n1 = n2
			n2 = soma
		}
		
		escreva("Os 15 primeiros elementos Fibonnaci são: \n")

		//Lê o Vetor
		para(inteiro i = 0; i < u.numero_elementos(fib); i++)
		{
			escreva("[", fib[i], "] ")
			u.aguarde(500)
		}

		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 549; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */