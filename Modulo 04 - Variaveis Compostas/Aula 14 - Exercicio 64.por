programa
{
	inclua biblioteca Util --> u
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 064 - Somador de Linas }\n\n")

  		inteiro mat[4][4]
		escreva("A matriz gerada foi\n")
		//Gerar a Matriz
		para(inteiro l = 0; l < u.numero_linhas(mat); l++)
		{
			para(inteiro c = 0; c < u.numero_colunas(mat); c++)
			{
				mat[l][c]	= sorteia(1, 10)
			}
		}

		//Exibi a Matriz
		para(inteiro l = 0; l < u.numero_linhas(mat); l++)
		{
			para(inteiro c = 0; c < u.numero_colunas(mat); c++)
			{
				escreva(mat[l][c] + "\t")	
			}	
			escreva("\n")
		}
		escreva("----------------------------------------")

		//Somando valor das linhas
		inteiro sL = 0
		para(inteiro c = 0; c < u.numero_colunas(mat); c++)
		{
			
			escreva("\nSomando a linha " + c + ": ")		
			escreva(mat[c][0] + " + " + mat[c][1] + " + " + mat[c][2] + " + " + mat[c][3])
			sL = mat[c][0] + mat[c][1] + mat[c][2] + mat[c][3]
			escreva(" = " + sL)
		}
		escreva("\n----------------------------------------")
		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 641; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */