programa
{
	inclua biblioteca Util --> u
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 060 - Analisando Números }\n\n")

		//Preencher vetor com valores sorteados
		escreva("Sorteando 10 valores...\n")
		inteiro vet[10]
		para(inteiro p = 0; p < u.numero_elementos(vet); p++)
		{
			vet[p] = sorteia(1, 10)
			escreva(vet[p] + ".. ")
		}
		escreva("\n-------------------------------------------")

		// Analisa valores pares
		escreva("\nAnalisando os valores sorteados...")
		escreva("\n---> Valores pares nas posições: " )
		inteiro valorP = 0, soma = 0
		para(inteiro p = 0; p < u.numero_elementos(vet); p++)
		{
			se(vet[p] % 2 == 0)
			{
				valorP = p
				escreva(valorP + " ")
				soma = soma + vet[p]
			}
		}
		escreva("\n\t---> Soma dos pares: "  + soma)
		
		// Analisa valores impares
		escreva("\n---> Valores impares nas posições: ")
		inteiro valorI = 0, totI = 0
		para(inteiro p = 0; p < u.numero_elementos(vet); p++)
		{
			se(vet[p] % 2 != 0)
			{
				valorI = p
				escreva(valorI + " ")
				totI++
			}
		}
		escreva("\n\t---> Quantidade dos ímpares: " + totI)

		// Descobre maior valor
		inteiro maior = 0      
		para(inteiro p = 0; p < u.numero_elementos(vet); p++)
		{
			se(p == 0)
			{
				maior = vet[p]
			}
			senao
			{
				se(vet[p] > maior)
				{
					maior = vet[p]
				}	
			}
		}
		escreva("\n---> Maior valor sorteado: " + maior)

		//Analisa maior valor
		inteiro tot = 0, valorM = 0
		escreva("\n\t---> Valor maior ocorreu nas posições: " )
		para(inteiro p = 0; p < u.numero_elementos(vet); p++)
		{
			se(vet[p] == maior)
			{
				escreva(p + " ")
				tot++
			}
		}
		escreva("\n\t---> Total de ocorrências: " + tot)
		escreva("\n-------------------------------------------")
		
		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1608; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */