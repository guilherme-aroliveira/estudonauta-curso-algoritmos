programa
{
	inclua biblioteca Util --> u
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 055 - O dobro do Vetor }\n\n")

		//Declaração das Variaveis
		inteiro vet[10]
		inteiro dobro = 3

		//percurso do Vetor
		para(inteiro i = 0; i < u.numero_elementos(vet); i++)
		{
			vet[i] = dobro
			dobro = dobro * 2	
		}
		escreva("O vetor foi gerado com os valores: \n")

		//leitura dos dados do Vetor
		para(inteiro i = 0; i < u.numero_elementos(vet); i++)
		{
			escreva(i, ":{", vet[i], "} ")
		}

		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 392; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */