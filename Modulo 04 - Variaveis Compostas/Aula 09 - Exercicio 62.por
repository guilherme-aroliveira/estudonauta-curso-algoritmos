programa
{
	inclua biblioteca Util --> u
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 062 - Listagem de Dados }\n\n")
		
		cadeia nome[6]
		caracter sexo[6]
		real salario[6]
		//Preenche os vetores com todos os dados
		para(inteiro p = 0; p < u.numero_elementos(nome); p++)
		{
			escreva("========== CADASTRO " + p + " ==========\n")
			escreva("Nome: ")
			leia(nome[p])
			escreva("Sexo [M/F]: ")
			leia(sexo[p])
			escreva("Salario: R$")
			leia(salario[p])
		}

		escreva("\nLISTAGEM COMPLETA")
		escreva("\n-------------------------------")
		escreva("\nNOME")
		escreva("\t\tSEXO")
		escreva("\tSALÁRIO")
		escreva("\n-------------------------------\n")

		//Exibi os dados armazenados nos vetores
		para(inteiro p = 0; p < u.numero_elementos(nome); p++)
		{
			
			escreva(nome[p])
			escreva("\t\t" + sexo[p])
			escreva("\tR$" + salario[p])
			escreva("\n")
		}
		escreva("-------------------------------")
		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 706; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = {nome, 9, 9, 4}-{sexo, 10, 11, 4}-{salario, 11, 7, 7};
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */