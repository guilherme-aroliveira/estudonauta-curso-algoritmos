programa
{
	inclua biblioteca Util --> u
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 056 - Vetor com Contagem 5 em 5 }\n\n")

		//Decalração das Variáveis
		inteiro vet[10]
		inteiro num

		//Entrada de Dados
		escreva("Me diga um valor: ")
		leia(num)

		//Percurso de Vetor
		para(inteiro i = 0; i < u.numero_elementos(vet); i++)
		{
			vet[i] = num
			num = num + 5
		}
		escreva("O vetor foi gerado com os valores: \n")

		para(inteiro i = 0; i < u.numero_elementos(vet); i++)
		{
			escreva(i, ":{", vet[i], "} ")
			u.aguarde(500)
		}

		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 537; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */