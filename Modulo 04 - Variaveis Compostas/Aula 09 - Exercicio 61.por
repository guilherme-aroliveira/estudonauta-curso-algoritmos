programa
{
	inclua biblioteca Util --> u
	inclua biblioteca Texto --> txt
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 061 - Analisando Nomes }\n\n")

		cadeia nome[6]
		inteiro tot = 0
		para(inteiro p = 0; p < u.numero_elementos(nome); p++)
		{
			escreva("Nome para posição [" + p + "]: ")
			leia(nome[p])
			tot++
		}
		escreva("\n===== " + tot + " NOMES CADASTRADOS COM SUCESSO =====")
		escreva("\n---------------- ANALISANDO ---------------")
		u.aguarde(500)
		
		escreva("\nNomes com menos de 6 letras: \n")
		para(inteiro p = 0; p < u.numero_elementos(nome); p++)
		{
			txt.numero_caracteres(nome[p])

			se(txt.numero_caracteres(nome[p]) < 6)
			{
				escreva(" [" + p + "]=" + nome[p])	
			}
		}
		escreva("\n---------------------------------------")
		
		escreva("\nNomes que começam com vogal: ")
		para(inteiro p = 0; p < u.numero_elementos(nome); p++)
		{
			txt.obter_caracter(nome[p], 1)
			
			se(txt.obter_caracter(nome[p], 1) == 'A')
			{
				escreva(" [" + p + "]=" + nome[p])	
			}
		}
		escreva("\n---------------------------------------")
		
		escreva("\nNomes que possuem letra S: ")
		para(inteiro p = 0; p < u.numero_elementos(nome); p++)
		{
			txt.posicao_texto(nome[p], nome[p], 0)
			
		}
		escreva("\n---------------------------------------")

		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1292; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */