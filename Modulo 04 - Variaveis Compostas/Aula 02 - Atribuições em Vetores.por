programa
{
	
	inclua biblioteca Util --> u
	funcao inicio()
	{
		inteiro idade[4] 

		para(inteiro pos = 0; pos < 4; pos++)	//preenche o vetor
		{
			//Preenche o vetor com valores aleatorios
			//idade[c] = sorteia(1,100)

			escreva("Digite uma idade: ")
			leia(idade[pos])
		}

		escreva("As idades digitadas foram ")

		para(inteiro pos = 0; pos < 4; pos++)	//lê o vetor
		{
			escreva(idade[pos], " -> ")	
			u.aguarde(500)
		}

		escreva("FIM\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 124; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */