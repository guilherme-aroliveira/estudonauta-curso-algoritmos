programa
{
	
	inclua biblioteca Util --> u
	funcao inicio()
	{
		inteiro idade[8] 

		para(inteiro pos = 0; pos < u.numero_elementos(idade); pos++)	//preenche o vetor de acordo com o seu tamanho
		{
			
			idade[pos] = sorteia(1,10)
		}

		escreva("As idades digitadas foram ")

		para(inteiro pos = 0; pos < u.numero_elementos(idade); pos++)	//lê o vetor
		{
			escreva(idade[pos], " -> ")	
			u.aguarde(500)
		}

		escreva("FIM\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 232; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */