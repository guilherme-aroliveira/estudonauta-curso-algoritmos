programa
{
	funcao inicio()
	{
		inteiro valor[5][2]
		para(inteiro l = 0; l < 5; l++) // linha
		{
			para(inteiro c = 0; c < 2; c++) // coluna
			{
				escreva("Digite um valor da posição [" + l+ "][" +c+"]: ")
				leia(valor[l][c])
			}	
		}
		escreva("FIM!")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 244; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */