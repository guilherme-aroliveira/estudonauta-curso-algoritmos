programa
{
	
	inclua biblioteca Util --> u
	inclua biblioteca Matematica --> mat
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 059 - Acima da Média }")

		escreva("\n---------------------------------")
		escreva("\n	ESCOLA ESTUDONAUTA 	  ")
		escreva("\n---------------------------------\n")

		//Preenche o vetor com as notas dos alunos
		real nota[6]
		para(inteiro p = 0; p < u.numero_elementos(nota); p++)
		{
			escreva("Nota do aluno " + p + ": ")
			leia(nota[p])
		}

		//Calcula a media das notas dos alunos
		real soma = 0, media
		para(inteiro p = 0; p < u.numero_elementos(nota); p++)
		{
			soma = soma + nota[p]	
		}
		media = soma / 6
		escreva("---------------------------------\n")
		escreva("A média da turma foi " + mat.arredondar(media, 1))
		escreva("\n---------------------------------")

		//Analise quais alunos ficaram acima da média da turma
		escreva("\nAlunos que ficaram acima da média: \n")
		para(inteiro p = 0; p < u.numero_elementos(nota); p++)
		{
			se(nota[p] > media)
			{
				escreva(p + " ")
			}
		}


		escreva("\n")
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 755; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */