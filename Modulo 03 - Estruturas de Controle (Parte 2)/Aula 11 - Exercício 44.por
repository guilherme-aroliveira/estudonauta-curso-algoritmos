programa
{
	/* Ex 044 - Programa que sorteia números, 
	 *  com uma condção de parada. Exibi os valores, 
	 *  a soma dos valores, o maior e menor valor 
	 *  e por ultimo exibi quantas vezes o numero 5 aparece.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	 
	inclua biblioteca Util --> u
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 044 - Números Sorteados }\n\n")

		inteiro num, valor=0, soma=0, cont5=0, menor=0, maior=0
		caracter resp

		escreva("Vou sortear vários números")
		escreva("\n----------------------------------------")

		faca
		{
			num = u.sorteia(1, 10)
			valor++
			soma += num
			
			escreva("\nO ", valor, "º sorteado foi: ", num)
			escreva("\nQuer sortear mais um? [S/N] ")
			leia(resp)

			se (valor == 1) // verifica quem é o primeiro número
			{
				maior = num
				menor = num
			}
			senao 
			{
				se (num < menor)
				{
					menor = num
				}
				se (num > maior)
				{
					maior = num
				}	
			}

			se(valor == 5)
			{
				cont5++
			}

			se(resp == 'N' ou resp == 'n')
			{
				pare	
			}

			
		} 
		enquanto(resp == 'S' ou resp == 's')

		escreva("\n=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
		escreva("\nVocê me fez sortear ", valor, " valores")
		escreva("\nA soma de todos eles foi igual a ", soma)
		escreva("\nO maior valor foi ", maior, " e o menor foi ", menor)
		escreva("\nO valor foi sorteado ", cont5, " vezes")

		
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 60; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */