programa
{
	/* Ex 045 - Programa para adivinhar valores sortedados 
	 *  de 1 a 10 em três chances. Cada chance exibi um mensagem diferente.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	 
	inclua biblioteca Util --> u
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 045 - Jogo de Adivinhar }\n\n")
		
		//Declaração de Variáveis
		inteiro total = 3, chance = 1, sorteio, palpite
		logico acertou = falso

		escreva("Vou pensar em um número entre 1 e 10")
		escreva("\nVocê tem 3 CHANCES para tentar adivinhar")
		escreva("\n---------------------------------------------")

		//Soteio de valores
		sorteio = u.sorteia(1, 10)

		faca
		{
			//Entrada de Dados
			escreva("\nChance de no. " + chance + " / " + total)
			escreva("\nEm que número pensei? ")
			leia(palpite)

			//Analisa a advinhação
			se (palpite == sorteio) 
			{
				acertou = verdadeiro
				escreva("\nPARABÉNS! Você acertou em " + chance + " tentativas!")
				pare
			}
			senao 
			{
				escreva("\nINFELIZMENTE ainda não foi dessa vez...")
				chance++
				u.aguarde(1000)
				
				se (chance <= total)
				{
					escreva("\nMas vou te dar outra chance...")
					u.aguarde(1000)

					se(palpite < sorteio)
					{
						escreva("\nChute um valor MAIOR!\n")
					}
					senao
					{
						escreva("\nChute um valor MENOR!\n")
					}
					u.aguarde(1000)
				}
				senao
				{
					escreva("\nSua chances ACABARAM!")
					pare
				}
				
			}	
			
		} enquanto (nao acertou)

		escreva("\n============= FIM DO JOGO =================")
		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1339; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */