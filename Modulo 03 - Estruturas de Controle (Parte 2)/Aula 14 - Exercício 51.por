programa
{
	inclua biblioteca Util --> u
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 051 - Triangulo }\n\n")

		inteiro andar, contA, contE
		inteiro tot = 1

		escreva("Quantos andares? ")
		leia(andar)

		para(contA = 1; contA <= andar; contA++)
		{
			para(contE= 1; contE <= tot; contE++)
			{
				escreva("*")
				u.aguarde(100)
			}
			tot++
			escreva("\n")	
		}
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 332; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */