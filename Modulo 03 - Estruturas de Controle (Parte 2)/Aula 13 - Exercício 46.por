programa
{
	/* Ex 046 - Programa que exibi a tabuada de qualquer 
	 *  número informado.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	 
	inclua biblioteca Util --> u
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 046 - Tabuada }\n\n")

		//Declaração das Variavéis
		inteiro n, i, valor = 0

		//Entrada de Dados
		escreva("NÚMERO = ")
		leia(n)

		//Criação da Tabuada
		para(i = 1; i <= 10; i++)
		{
			valor = n * i
			escreva(n, " x ", i, " = ", valor, "\n")
			u.aguarde(300)
		}
		escreva("------- FIM ---------")
		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 381; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */