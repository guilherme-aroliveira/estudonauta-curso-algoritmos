programa
{
	
	funcao inicio()
	{
		// Declaração de Variáveis 
		inteiro c = 1, meninos = 0, meninas = 0 
		caracter sx

		// Iteração
		enquanto (c <= 5)
		{
			escreva("Digite o sexo: [M/F] ")
			leia(sx)

			// Condição
			se (sx == 'M' ou sx == 'm')
			{
				meninos++
			}

			se (sx == 'F' ou sx == 'f')
			{
				meninas++
			}
			
			c++
		}

		escreva("\nO total de meninos é ", meninos)
		escreva("\nO total de meninas é ", meninas)
		
		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 342; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */