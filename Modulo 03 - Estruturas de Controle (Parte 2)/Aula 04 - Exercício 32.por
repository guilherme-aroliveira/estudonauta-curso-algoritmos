programa
{
	/* Ex 032 - Programa que lê 5 valores e 
	 *  retorna a soma dos valores pares e ímpares.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta 
	 */
	 
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 32 - Soma Par e Ímpar }")

		// Declaração de Variáveis
		inteiro n1, somaP=0, somaI=0, i=1

		escreva("\n\n")

		// Iteração
		enquanto (i <= 5)
		{
			escreva("Digite o " + i + "º valor: ")
			leia(n1)
			
			se (n1 % 2 == 0)
			{
				somaP = somaP + n1	
			}
			senao
			{
				somaI = somaI + n1
			}	
				
			i++
		} 

		//Saída dos Resultados
		escreva("----------------------------------")
		escreva("\nSomando todos os pares, temos: " + somaP)
		escreva("\nSomando todos os ímpares, temos: " + somaI)

		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 560; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */