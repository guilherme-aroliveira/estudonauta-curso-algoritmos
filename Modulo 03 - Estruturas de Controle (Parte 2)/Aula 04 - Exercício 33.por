programa
{
	/* Ex 033 - Programa que lê, sorteia a quantidade de números informados 
	 *  e retorna a soma de todos os valores sorteados.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	 
	inclua biblioteca Util --> u
	 
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 33 - Sorteio de Números }")

		// Declaração das Variáveis
		inteiro n1, i=1, num, soma=0

		// Entrada de Dados
		escreva("\n\nQuantos números você quer que eu sorteie: ")
		leia(n1)
		escreva("--------------------------------------------")

		// Estrutura de repetição
		enquanto (i <= n1)
		{
			num = u.sorteia(1, 10)
			escreva("\nO " + i + "º valor sorteado foi: " + num)	
			soma = soma + num
			i++
		}

		// Saída dos Resultados
		escreva("\n--------------------------------------------")
		escreva("\nSomando todos os valores, temos: " + soma)

		escreva("\n\n")
	}
	
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 24; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */