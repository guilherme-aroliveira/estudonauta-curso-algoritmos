programa
{
	/* Ex 050 - Programa que exibi a tabuada a partir 
	 *  dos dados que o usuário inseriu.
	 * Autor: Guilherme Oliveira
	 * Insituição: Estudonauta
	 */
	 
	inclua biblioteca Util --> u
	 
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 050 - Tabuadas }\n\n")

		//Declaração de Variavéis
		inteiro ti, tf, valor = 0

		//Entrada de Dados
		escreva("Tabuada INICIAL = ")
		leia(ti)
		escreva("Tabuada FINAL = ")
		leia(tf)

		//Criação das tabuadas
		para(inteiro x = ti; x <= tf; x++)
		{
			escreva("\n--------------------")
			escreva("\nTABUADA de ", x)
			escreva("\n--------------------\n")

			para(inteiro y = 1; y <= 10; y++)
			{
				valor = x * y
				escreva(x + " x " + y + " = " + valor + "\n")
				u.aguarde(300)
			}	
		}
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 454; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */