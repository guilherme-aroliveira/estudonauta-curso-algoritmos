programa
{
	/* Ex 035 - Programa que lê o peso e o sexo do número
	 *  de pessoas cadastradas. E retorna a quantidade de homens e mulheres
	 *  acima do peso de referência, definido pelo usuário.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 35 - Pessoas }")

		// Declaração de Variavéis
		inteiro pessoas, i = 1, pesoA = 0, h = 0, m = 0
		real peso, pesoP
		caracter sexo

		//Entrada de Dados
		escreva("\n\nQuantas pessoas vamos cadastrar? ")
		leia(pessoas)
		escreva("Qual é o peso de referência(Kg)? ")
		leia(peso)

		//Estutura de repetição
		enquanto(i <= pessoas)
		{
			escreva("\n-------------------------------")
			escreva("\nPESSSOA " + i + " de " + pessoas)
			escreva("\n-------------------------------")
			escreva("\nPESO: (kg) ")
			leia(pesoP)
			escreva("SEXO: [M/F] ")
			leia(sexo)

			se (sexo == 'M' ou sexo == 'm')
			{
				h++	
			}
			se (sexo == 'F' ou sexo == 'f')
			{
				m++
			}

			se (pesoP <= peso)
			{
				escreva("======= PESO DENTRO DO LIMITE ("+ peso +"Kg) =======")
			}
			senao
			{
				escreva("======= PESO ACIMA DO LIMITE ("+ peso +"Kg) =======")
				pesoA++
				
			}

			i++
		}

		escreva("\n--------------------------------------------")
		escreva("\nAo todo, temos " + pesoA + " pessoas acima do limite de " + peso + "Kg.")
		escreva("\nE dessas pessoas, " + h +  " são HOMENS e " + m + " são MULHERES.")
		
		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 195; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */