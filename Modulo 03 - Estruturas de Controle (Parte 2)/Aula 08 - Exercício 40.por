programa
{
	/* Ex 040 - Programa que lê dois valores, seleciona uma opção 
	 *  e realiza operações.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta 
	 */
	inclua biblioteca Util --> u
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 40 - Calculadora }")
		escreva("\n\n")

		// Declaração de Variavéis
		inteiro op1, op2, option=0

		// Entrada de Dados
		escreva("Operando 1: ")
		leia(op1)
		escreva("Operando 2: ")
		leia(op2)

		// Iteração
		enquanto (option != 5)
		{
			escreva("\n========== ESCOLHA UMA OPERAÇÃO =========== ")
			escreva("\n[ 1 ] Adiçao")
			escreva("\n[ 2 ] Subtração")
			escreva("\n[ 3 ] Multiplicação")
			escreva("\n[ 4 ] Entrar com novos dados")
			escreva("\n[ 5 ] Sair")
			escreva("\n>>>>> SUA OPÇÃO: ")
			leia(option)

			// Operações e Saída dos Resultados
			escolha (option)
			{
				caso 1: //Adição
					escreva("\n---------------------------")
					escreva("\nCalculando ", op1, " + ", op2, " = ", (op1 + op2))
					escreva("\n---------------------------")
					pare		
				
				caso 2: // Subtração
					escreva("\n---------------------------")
					escreva("\nCalculando ", op1, " - ", op2, " = ", (op1 - op2))
					escreva("\n---------------------------")	
					pare	
				
				caso 3:// Multiplicação
					escreva("\n---------------------------")
					escreva("\nCalculando ", op1, " * ", op2, " = ", (op1 * op2))
					escreva("\n---------------------------")
					pare		
				
				caso 4: // Entrar Dados
					escreva("\nOperando 1: ")
					leia(op1)
					escreva("Operando 2: ")
					leia(op2)	
					pare	

				caso 5: // Sair
					escreva("\n====== SAINDO ====== ")
					pare	

				caso contrario: // Opção diferente
					escreva("\n======= OPÇÃO INVÁLIDA ========\n")
					pare	
			
				
			}
			u.aguarde(1000)
		}
		
		escreva("\n====== VOLTE SEMPRE ====== ")
		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 100; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */