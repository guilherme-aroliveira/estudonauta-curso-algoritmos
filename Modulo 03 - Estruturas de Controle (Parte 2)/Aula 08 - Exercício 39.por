programa
{
	/* Ex 039 - Programa que lê valores e após digitar 9999 
	 *  encerra a iteração e retorna no final a quantidade 
	 *  de valores digitados, a média, a soma, 
	 *  e o maior valor entre eles.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	inclua biblioteca Tipos --> t
	inclua biblioteca Matematica --> m
	 
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 039 - Lendo Dados }")
		escreva("\n")

		// Declaração das Variaveis
		inteiro c=0, num = 0, soma=0, maior = 0
		real media

		// Iteração
		enquanto(num != 9999)
		{
			// Entrada de Dados
			escreva("\n--------------------------------------")
			escreva("\n"  + (c + 1) + "° Valor [Digite 9999 para encerrar]")
			escreva("\n--------------------------------------")
			escreva("\nNÚMERO: ")
			leia(num)

			//Análise de Dados
			se(num != 9999)
			{
				soma += num

				se (c == 0)
				{
					maior = num
				}
				senao
				{
					se(num > maior)
					{
						maior = num
					}	
				}
				c++				
			}
		}
		
		media = t.inteiro_para_real(soma) / c 

		// Saída dos Resultados
		escreva("\n========== FLAG DIGITADO ============")
		escreva("\nAo todo você digitou ", c, " valores")
		escreva("\nA soma entre eles foi: ", soma)
		escreva("\nE a média foi: ", m.arredondar(media, 2))
		escreva("\nO maior valor digitado foi: ", maior)

		escreva("\n")	
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 118; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */