programa
{
	/*
	 * 
	 */
	
	funcao inicio()
	{
		inteiro c = 1

		enquanto (c <= 6) // teste lógico no início
		{
			escreva(c + " - ")

			c = c + 1 // c += 1 ou c++
		}

		escreva("FIM!")

		escreva("\n\n")
		escreva("COMEÇOU - ")

		inteiro c1 = 10
		
		enquanto (c1 >= 0)
		{
			escreva(c1 + " - ")

			c1 = c1 - 1
		}

		/* Tudo que estiver fora do "enquanto", acontece
		 * só uma vez. 
		 */

		escreva("FIM!")
		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 114; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */