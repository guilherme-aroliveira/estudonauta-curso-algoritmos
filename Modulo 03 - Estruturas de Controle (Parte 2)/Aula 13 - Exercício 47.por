programa
{
	/* Ex 047 - Programa que realiza uma contagem a partir
	 *  dos valores informados pelo usuário.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	 
	inclua biblioteca Util --> u
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 047 - Contagem Personalizada }\n\n")

		//Declaração das Variavéis
		inteiro ini, final, passo

		//Entrada de Dados
		escreva("INÍCIO = ")
		leia(ini)
		escreva("FINAL = ")
		leia(final)
		escreva("PASSO = ")
		leia(passo)

		se(passo <= 0) passo *= -1 

		//Contagem crescente
		se(ini < final)
		{
			para(inteiro c = ini; c <= final; c += passo)
			{
			 	escreva(c + "... ")
			 	u.aguarde(300)
			}
		}
		//Contagem decrescente
		senao
		{
			para(inteiro c = ini; c >= final; c -= passo) 
			{
				escreva(c + "... ")
			 	u.aguarde(300)
			}	
		}

		escreva("ACABOU!")
		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 673; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */