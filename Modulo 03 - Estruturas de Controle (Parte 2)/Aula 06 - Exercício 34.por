programa
{
	/* Ex 034 - Programa que lê 5 valores, retorna a quantidade
	 *  de números pares e ímpares e exibi o valor da média de cada um.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */

	 inclua biblioteca Tipos --> t
	 inclua biblioteca Matematica --> m
	 
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 34 - Pares e Ímpares }")	

		// Declaração das Variáveis
		inteiro num, c = 1, sp = 0, si = 0, np = 0, ni = 0
		real mediaP, mediaI

		escreva("\n\n")

		// Estrutura de Repetição
		enquanto (c <= 5)
		{
			escreva("Escreva o " + c + " º valor: ")
			leia(num)

			// Estrutura Condicional
			se (num % 2 == 0)
			{
				np++
				sp = sp + num
			}
			senao
			{
				ni++
				si += num
			}

			c++
		}

		mediaP = t.inteiro_para_real(sp) / np
		mediaI = t.inteiro_para_real(si) / ni

		escreva("------------------------------------")

		// Saída dos Resultados
		escreva("\n\nVocê digitou " + np + " números pares. A média é: " + m.arredondar(mediaP, 2))
		escreva("\nVocê digitou " + ni + " números ímpares. A média é: " + m.arredondar(mediaI, 2))

		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 21; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */