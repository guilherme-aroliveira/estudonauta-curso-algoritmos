programa
{
	/* Ex 041 - Programa que lê o nome e a idade de novos amigos,
	 *  exibi o total de amigos cadastrados, exibi a média da idades,
	 *  exibi o amigo mais velho e mais novo com a suas idades.
	 *  O programa possui uma clausula de parada.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	 
	inclua biblioteca Tipos --> t
	inclua biblioteca Texto --> tx 
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 41 - Cadastro de Amigos }\n")

		//Declaração de Variáveis
		inteiro idade, tot = 0, mj = 0, mv = 0, soma = 0
		cadeia nome, velho = "", novo = ""
		real media

		//Iteração
		enquanto(verdadeiro)
		{
			escreva("\n------------- NOVO AMIGO --------------")
			escreva("\nOBS: Digite ACABOU no nome para parar")
			escreva("\nNome: ")
			leia(nome)

			se(tx.caixa_alta(nome) == "ACABOU")
			{
				pare
			}
			
			escreva("Idade: ")
			leia(idade)

			//Analisando os dados
			tot++
			soma += idade

			se (tot == 1)
			{
				mv = idade
				velho = nome
				mj = idade
				novo = nome
			}
			senao
			{
				se(idade < mj)
				{
					mj = idade
					novo = nome
				}	
				se(idade > mv)
				{
					mv = idade
					velho = nome
				}
			}
				
		}
		
		media = t.inteiro_para_real(soma) / tot
		
		escreva("\n*********** INTERROMPIDO **********")
		escreva("\n============ RESULTADOS =============")

		//Saída dos Resultados
		escreva("\n\nTotal de amigos cadastrados: ", tot)
		escreva("\nA média de idade do grupo é de ", media)
		escreva("\nSeu amigo mais velho é "  + velho + " com ", mv, " anos")
		escreva("\nSeu amigo mais jovem é " + novo + " com ", mj,  " anos")
		

		escreva("\n")
		
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 215; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */