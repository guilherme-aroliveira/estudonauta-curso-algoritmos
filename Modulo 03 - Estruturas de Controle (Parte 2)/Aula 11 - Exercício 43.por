programa
{
	/* Ex 042 - Programa que lê vários valores, exibi a quantidade
	 *  de valores, a quantidade de números pares e o menor número
	 *  impar digitado. Possui uma clausula de parada.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	 
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 043 - Analisador de Números }\n\n")

		inteiro valor=0, num, par=0, totImp = 0, menorImp = 0
		caracter resp

		faca
		{
			escreva("Digite o ", (valor + 1), "º valor: ")
			leia(num)
			escreva("Quer continuar? [S/N]")
			leia(resp)

			se(num % 2 == 0)
			{
				par++
			}
			senao
			{
				se(totImp == 1)
				{
					menorImp = num
				}
				senao
				{
					se(num < menorImp)
					{
						menorImp = num
					}
				}
			}	

			valor++
		}
		
		enquanto (resp == 'S' ou resp == 's')

		escreva("\n-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")	
		escreva("\nAo todo você digitou ", valor, " valores.")
		escreva("\nVocê digitou ", par, " valores PARES.")
		escreva("\nO valor ", menorImp, " foi o menor número ÍMPAR digitado.")

		escreva("\n")
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 190; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */