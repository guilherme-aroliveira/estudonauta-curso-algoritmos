programa
{
	/* Ex 036 - Programa que sorteia a quantidade de números informada
	 *  e que retorna o total de números informados, além de retornar
	 *  a quantidade de números maiores que 5 e divisíeis por 3. 
	 * Autor: Guilherme Oliveira 
	 * Instituição: Estudonauta
	 */

	inclua biblioteca Util --> u
	 
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 36 - Analisando números }")

		// Declaração de Variáveis
		inteiro n, i = 1, sor, maior = 0, div = 0

		// Entrada de dados
		escreva("\n\nQuantos números você que sortear: ")
		leia(n)
		escreva("----------------------------------------")
		escreva("\nSorteando " + n + " números... \n")

		// Estrutura de repetição
		enquanto (i <= n)
		{
			sor = u.sorteia(1, 10)	
			escreva( sor + ".. ")

			se(sor > 5)
			{
				maior++
			}
			se(sor % 3 == 0)
			{
				div++
			}
			
			i++
		}
		escreva("PRONTO!")
		escreva("\n----------------------------------------")

		// Saída dos Resultados
		escreva("\nDos " + n + " números sorteados")
		escreva("\n" + maior + " são maiores que cinco")
		escreva("\ne " + div + " são divisíveis por três.")

		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 718; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */