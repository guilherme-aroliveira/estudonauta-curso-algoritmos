programa
{
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 053 - Números Validos }\n\n")

		inteiro num, total = 0, valor = 0
		caracter resp

		para (valor = 1; valor <= 3; valor++)
		{
			escreva("-------------------------------------")
			escreva("\n            VALOR ", valor)
			escreva("\n-------------------------------------")
			
			enquanto(verdadeiro)
			{
				escreva("\nDigite um número (entre 1 e 10): ")
				leia(num)
	
				se(num > 0 e num <= 10)
				{
					pare
				}
				senao 
				{
					escreva("<<ERRO> O número de estar entre 1 e 10!")	
				}
				
				escreva("\nQuer continuar? [S/N]")
				leia(resp)
	
				se(resp == 'N' ou resp == 'n')
				{
					pare
				}	
				senao
				{
					escreva("\n<<ERRO>> Resposta inválida, tente novamente!")
				}
				
				total += num
			}
			 
		}
		escreva("\n\n-=-=-=-=-=-=-=-=RESULTADO-=-=-=-=-=-=-=-")
		escreva("\nAo todo, você digitou ", valor, " valores")
		escreva("\nA soma de todos eles foi ", total)

		escreva("\n")
		
		
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 276; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */