programa
{
	/* Ex 037 - Programa que o nome e idade de várias pessoas
	 *  definidas anteriormente pelo usuário. E depois, retorna
	 *  a pessoa mais velha e a mais jovem.
	 * Autor: Guilherme Oliveira 
	 * Instituição: Estudonauta
	 */
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 037 - Mais velho e mais novo }")

		// Declaração de Variáveis
		inteiro idade, i = 1, nPessoas, mV = 0, mJ = 0
		cadeia nome, pJovem="", pVelha=""

		// Entrada de Dados
		escreva("\n\nEscreva o número de pessoas: ")
		leia(nPessoas)
		

		// Repetição
		enquanto(i <= nPessoas)
		{
			escreva("\n-------------")
			escreva("\n" + i + "a PESSOA ")
			escreva("\n-------------")
			escreva("\nNOME: ")
			leia(nome)
			escreva("IDADE: ")
			leia(idade)

			se(i == 1)
			{
				mV = idade
				pVelha = nome
				mJ = idade
				pJovem = nome
			}
			senao
			{
				se(idade > mV)
				{
					mV = idade
					pVelha = nome
				}
				se(idade < mJ)
				{
					mJ = idade
					pJovem = nome
				}
			}
			i++
		}
		
		// Saída dos Resultados
		escreva("\n==============================================")
		escreva("\nA pessoa mais jovem é ", pJovem, " que tem ", mJ, " anos")
		escreva("\nA pessoa mais velha é ", pVelha, " que tem ", mV, " anos")

		escreva("\n")

		
	} 
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 68; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */