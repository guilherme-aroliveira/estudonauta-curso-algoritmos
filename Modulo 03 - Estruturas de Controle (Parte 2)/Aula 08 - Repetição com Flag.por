programa
{
	
	funcao inicio()
	{
		inteiro idade = 0, soma = 0

		enquanto (idade != 9999)
		{
			escreva("Digite sua idade: ")
			leia(idade)

			se(idade != 9999)
			{
				soma += idade
			}
			
		}

		escreva("\nA soma de todas as idade é: ", soma)
		escreva("\n========= Programa Encerrado =========")

		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 174; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */