programa
{
	
	funcao inicio()
	{
		inteiro x, y

		para(x = 1; x <= 3; x++)
		{
			para(y = 1; y <= 2; y++)
			{
				escreva(x, " ", y, "\n")
			}	
		}

		escreva("\n=========================\n\n")

		inteiro a = 1, b = 1

		enquanto(a <= 3)
		{
			b = 1
			enquanto(b <= 2)
			{
				escreva(a, " ", b, "\n")
				b++
			}
			a++
		}
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 195; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */