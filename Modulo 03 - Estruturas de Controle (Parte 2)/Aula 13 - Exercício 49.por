programa
{	
	/* Ex 049 - Programa que lê o número de elementos 
	 *  informados pelo usuário e exibi a série de fibonacci.
	 * 	Autor: Guilherme Oliveira
	 * 	Instituição: Estudonauta
	 */
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 049 - Sequência de Fibonacci }\n\n")

		//Declaração de Variavéis
		inteiro num, n1 = 0, n2 = 1, n3
		
		//Entrada de Dados
		escreva("Quantos elementos você quer exibir? ")
		leia(num)

		escreva(n1 + " ")
		escreva(n2 + " ")

		//Realiza a sequência de Fibonacci
		para (inteiro i = 3; i <= num; i++)
		{
			n3 = n1 + n2
			escreva(n3 + " ")
			n1 = n2
			n2 = n3
		}

		escreva(" PRONTO!")
		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 183; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */
