programa
{
	
	inclua biblioteca Util --> u
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 052 - Piramide }\n\n")

		inteiro andar, quantEsp = 0
		
		escreva("Quantos andares a pirâmide irá ter? ")
		leia(andar)

		inteiro quantEst = (andar * 2) - 1

		para(inteiro cAnd = 1; cAnd <= andar; cAnd++)
		{
			para(inteiro cEsp= 1; cEsp <= quantEsp; cEsp++)
			{
				escreva(" ")
			}	
			quantEsp++

			para(inteiro cEst = 1; cEst <= quantEst; cEst++)
			{
				escreva("*")
				u.aguarde(100)	
			}
			escreva("\n")
			quantEst -= 2
		}
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 339; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */