programa
{
	/* Ex 042 - Programa que lê o nome, sexo e salario de um pessoa.
	 *  Exibi o total de pessoas cadastradas, o total de homens,
	 *  o total de mulheres, a media salarial dos homens, o total
	 *  de mulheres que ganham acima de R$1000 e por último exibi 
	 *  o maior salário entre os homens. Possui uma clausula de parada.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 42 - Cadastro de Funcionários }\n\n")

		//Declaração de Variavéis
		cadeia nome
		caracter sexo, resp
		inteiro tot = 0, totH = 0, totM = 0, totMil = 0
		real salario, somaSalH = 0, medSalH = 0, maiH = 0
		
		

		enquanto(verdadeiro)
		{
			//Entrada de Dados
			escreva("Nome: ")
			leia(nome)
			escreva("Sexo [M/F]: ")
			leia(sexo)
			escreva("Salário: R$")
			leia(salario)

			//Analise de Dados
			tot++
			se (sexo == 'M' ou sexo == 'm')
			{
				totH++
				somaSalH += salario

				se (totH == 1)
				{
					maiH = salario
				}
				senao se (salario > maiH)
				{
					maiH = salario
				}
			}
			senao se(sexo == 'F' ou sexo == 'f')
			{
				totM++
				
				se (salario > 1000)
				{
					totMil++	
				}
			}

			//Clausula de Parada
			escreva("Quer continuar? [S/N] ")
			leia(resp)

			se(resp == 'N' ou resp == 'n')
			{
				pare
			}
			escreva("--------------------------------\n")
		}

		medSalH = salario / totH 
		
		escreva("========== RESULTADOS ==========\n")

		//Saída dos Resultados
		escreva("\nTotal de pessoas cadastradas: ", tot)
		escreva("\nTotal de homens: ", totH)
		escreva("\nTotal de mulheres: ", totM)
		escreva("\nMédia salarial dos homens: R$", medSalH)
		escreva("\nTotal de mulheres que ganham mais de Mil Reais: ", totMil)
		escreva("\nMaior salário entre os homens: R$" + maiH)

		escreva("\n")
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 321; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */