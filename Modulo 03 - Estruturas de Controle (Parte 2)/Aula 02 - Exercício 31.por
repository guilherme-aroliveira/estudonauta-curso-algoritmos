programa
{
	/* Ex 031 - Programa que lê dois valores, realiza uma contagem regressiva
	 *  e marca os múltiplos do valor informado.
	 * Autor: Guilherme Oliveira 
	 * Instituição: Estudonauta
	 */
	 
	inclua biblioteca Util --> u
		
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 31 - Contagem Regressiva }")

		// Declaração de Variáveis
		inteiro cont, mult

		// Entrada de Dados
		escreva("\n\nSua contagem regressiva vai comecar em: ")
		leia(cont)
		escreva("Marcar os múltiplos de: ")
		leia(mult)

		escreva("\n")

		// Iteração e Saída dos Resultados
		enquanto (cont >= 0)
		{
			se (cont % mult == 0)
			{
				escreva("[" + cont + "] - ")
			}
			senao
			{
				escreva(cont + " - ")
			}
			u.aguarde(900)
			cont--
		}

		escreva("FIM!")
		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 744; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */