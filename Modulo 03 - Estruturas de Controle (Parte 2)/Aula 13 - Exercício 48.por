programa
{
	/* Ex 048 - Programa que lê um número inteiro qualquer e verifica 
	 *  se ele é um número primo ou não, alêm de marcar os números que são
	 *  divisiveis por ele.
	 *  Autor: Guilherme Oliveira
	 *  Instituição: Estudonauta	
	 */
	
	funcao inicio()
	{
		escreva("{ EXRCÍCIO 048 - Número Primo }\n\n")

		//Declaração de Variavéis
		inteiro n, divi = 0

		//Entrada de Dados
		escreva("Digite um número: ")
		leia(n)

		para (inteiro i = 1; i <= n; i++)
		{
			se(n % i == 0)
			{
				escreva("[" + i + "] ")
				divi++
			}
			senao
			{
				escreva(i + " ")	
			}
				
		}
		escreva("\n")

		//Verifica se o número é primo
		se(divi <= 2)
		{
			escreva("\nO número ", n, " foi divisivel " + divi + " vezes")
			escreva("\nLogo, ele é PRIMO!")	
		}
		senao
		{
			escreva("O número ", n, " foi divisivel " + divi + " vezes")
			escreva("\nLogo ele NÃO É PRIMO!")
		}

		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 166; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */