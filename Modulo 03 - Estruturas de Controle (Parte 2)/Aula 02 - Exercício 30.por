programa
{
	/* Ex 030 - Programa que realiza uma contagem e substitui todos
	 *  os elementos que pertence a tabuada de 4 pela palavra PIN.
	 * Autor: Guilherme Oliveira  
	 * Insituição: Estudonauta
	 */
	 
	inclua biblioteca Util --> u
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 30 - Jogo do Pin }")

		// Declaraçao de Variáveis
		inteiro n1, i = 1

		// Entrada de Dados
		escreva("\n\nQuer contar até quanto? ")
		leia(n1)

		
		// Iteração e Saída de resultados
		enquanto (i <= n1)
		{
			se (i % 4 == 0)
			{
				escreva("PIN! ")
			}
			senao
			{	
				escreva(i + " - ")
			}

			u.aguarde(300)
			i++
		}

		escreva("FIM!")
		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 469; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */