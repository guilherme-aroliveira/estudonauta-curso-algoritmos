programa
{
	/* Ex 029 - Programa que lê três valores e realiza uma contagem
	 * Autor: Guilherme Oliveira
	 * Insituição: Estudoauta
	 */

	 inclua biblioteca Util --> u
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 20 - Contagem Personalizada }")

		// Declaração de Variáveis
		inteiro n1, n2, i

		// Entrada de Dados
		escreva("\n\nOnde começa a contagem? ")
		leia(n1)
		escreva("Onde termina a contagem? ")
		leia(n2)
		escreva("Qual vai ser o incremento? ")
		leia(i)

		 

		// Iteração e Saída dos Resultados
		enquanto (n1 <= n2)
		{
			escreva(n1 + " - ")
			u.aguarde(1000)
			n1 = n1 + i
		}

		escreva("FIM!")

		escreva("\n\n")		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 105; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */