programa
{
	/* Ex 013 - Programa que lê duas notas de um aluno, calcula a média 
	 *  e mostre uma mensagem de acordo com o valor da média.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 013 - Bons alunos merecem parabéns }")

		// Declaração das Variáveis
		real n1, n2, media

		// Entrada de Dados
		escreva("\n\nDigite a sua primeira nota: ")
		leia(n1)
		escreva("Digite a sua segunda nota: ")
		leia(n2)

		// Cálculos
		media = (n1 + n2) / 2

		// Condição
		se (media >= 7)
		{
			escreva("MEUS PARABÉNS! ")	
		}

		// Saída dos Resultados
		escreva("Sua média final foi de: ", media)

		escreva("\n\n")

		 
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 348; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */