programa
{
	/* Ex 016 - Programa que lê o ano de nascimento, mostra a idade
	 *  e cexibida uma mensagem na tela 
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */

	 inclua biblioteca Calendario --> c
	 
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 016 - Serviço Militar v1.0}")

		// Declaração das Variáveis
		inteiro anoNasc, idade
		inteiro ano = c.ano_atual() 

		// Entrada de Dados
		escreva("\n\nEm que ano você nasceu? ")
		leia(anoNasc)
		escreva("--------------------------------------")

		// Cálculos
		idade = ano - anoNasc

		// Saída dos Resultados
		escreva("\nSua idade atual é de ", idade, " anos.")
		
		//Condição e saída dos resultados
		se (idade >= 18)
		{	
			escreva("\nEspero sinceramente que você tenha se alistado.")
		}
		senao
		{
			escreva("\nVocê ainda não completou 18 anos. Não pode se alistar")	
		}

		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 577; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */