programa
{
	/* Ex 014 - Programa que lê um valor, calcula 10% de desconto 
	 *  caso a compra seja maior de R$500 e retorna o valor final com o desconto
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 014 - Consumidor ganha 10% de desconto }")

		// Declaração das Variáveis
		real valor

		// Entrada de Dados 
		escreva("\n\n Qual foi o valor total das suas compras? R$")
		leia(valor)
		escreva("-------------------------------------------")

		// Saída dos Resultados
		escreva("\n Você comprou R$", valor, " na nossa loja. Obrigado!")

		// Cáculos
		real desc = (valor * 10) / 100

		// Condição
		se (valor > 500)
		{
			escreva("\n\n ===== ATENÇÃO ===== ")
			escreva("\n Por fazer mais de R$", valor, " em compras, você vai receber R$", desc, " de desconto.")
			escreva("\n O valor a ser pago é de R$", (valor - desc), "! Volte sempre!")
		}

		escreva("\n\n")
		
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 647; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */