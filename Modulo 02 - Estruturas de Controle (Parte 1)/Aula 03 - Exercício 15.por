programa
{
	/* Ex 015 - Programa que lê a idade e caso a idade
	 *  for maior que 65 anos exibe um mensagem
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */

	 inclua biblioteca Calendario --> c
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 015 - Fila de Banco }")

		// Declaração das Variáveis
		inteiro anoNasc, idade
		// inteiro anoatual = c.ano_atual()

		// Entrada de Dados 
		escreva("\n\nEm que ano você nasceu? ")
		leia(anoNasc)

		// Cáculos
		idade = c.ano_atual() - anoNasc

		//Saída dos Resultados
		escreva("Você tem " + idade + " anos, certo? Seja bem-vindo(a) ao Banco Estudonauta!")

		// Condição
		se (idade >= 65)
		{
			escreva("\n\n===== ATENÇÃO! DIRIJA-SE PARA FILA PREFERENCIAL ======")
		}

		escreva("\n\n")

		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 618; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */