programa
{
	
	funcao inicio()
	{
	// Estrutura com número inteiro
		inteiro num
		
		escreva("Digite um número inteiro pequeno: ")
		leia(num)

		escolha(num)
		{
			caso 1:
				escreva(" Um ")
				pare
			caso 2:
				escreva(" Dois ")
				pare
			caso 3:
				escreva(" Três ")
				pare
			caso 4:
				escreva(" Quatro ")
				pare
			caso contrario:
				escreva(" Erro ")
				pare		
		}

		escreva("\n\n")

	// Estrutura com caractere 
		caracter letra 

		escreva("Escreva um letra qualquer: ")
		leia(letra)

		escolha(letra)
		{
			caso 'a': caso 'A':
				escreva(" Primeira letra ")
				pare
			caso 'b': caso 'B':
				escreva(" Primeira Consoante ")
				pare
			caso contrario:
				escreva(" Não registrei essa letra ainda ")
				pare		
		}

		escreva("\n\nFIM DO PROCESSAMENTO ")	

		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 711; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */