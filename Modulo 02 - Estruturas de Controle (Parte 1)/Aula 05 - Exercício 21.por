programa
{

	/* Ex 021 - Programa que lê um numero e verifica
	 *  se ele é negativo ou positivo
	 *  a pessoa não pode comprar.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 021 - Positivo ou Negativo }")

		// Declaração de Variáveis
		inteiro n1

		// Entrada de Dados
		escreva("\n\nDigite um número: ")
		leia(n1)

		/* Recomendação do Professor
		 *  escreva("O número " + n1 + " digitado é ")
		 *  
		 *  se (n1 > 0)
		 *  {
		 *  		escreva("POSITIVO")
		 *  }
		 *  senao se (n1 < 0)
		 *  		{
		 *  			escreva("NEGATIVO")
		 *  		}
		 *  		senao
		 *  		{
		 *  		 	escreva("NULO")
		 *  		}
		 */

		// Condição e Saída de Dados
		se (n1 > 0)
		{
			escreva("Você digitou um número POSITIVO")
		}
		senao se (n1 < 0) 
			{
				escreva("Você digitou um número NEGATIVO")
			}
			senao
			{
				escreva("Você digitou um número NULO")
			}

		escreva("\n\n")	
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 652; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */