programa
{

	/* Ex 022 - Programa que lê dois números e
	 *  coloca em ordem crescente
	 *  a pessoa não pode comprar.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 022 - Ordem crescente }")

		//Declaração de Variáveis
		inteiro n1, n2

		// Entrada de Dados
		escreva("\n\nDigite um número: ")
		leia(n1)
		escreva("Digite outro número: ")
		leia(n2) 

		//Condição
		se (n1 < n2)
		{
			escreva("Os números em ordem são: " + n1 + " e " + n2)
		}
		senao se (n1 > n2)
			{
				escreva("Os números em ordem são: " + n2 + " e " + n1)
			}
			senao
			{
				escreva("Não tem como colocar esses valores em ordem. Eles são iguais")
			}

		escreva("\n\n")
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 536; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */