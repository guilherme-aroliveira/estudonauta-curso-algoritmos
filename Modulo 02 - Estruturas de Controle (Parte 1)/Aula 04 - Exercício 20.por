programa
{
	/* Ex 020 - Programa que lê a hora atual e quanto de dinheiro
	 *  a pessoa têm, se for menos que o preço do ingresso,
	 *  a pessoa não pode comprar.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */

	 inclua biblioteca Calendario --> c
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 020 - Dá pra ver o filme? }")

		// Declaração de Variáveis
		inteiro money, hora
		 

		// Cabeçalho do Programa
		escreva("\n\n======== CINEMA ESTUDONAUTA ========")
		escreva("\n\nHORÁRIO DO FILME: 12h - PREÇO DO INGRESSO: R$ 20")
		escreva("\n---------------------------------------------------")
		
		// Entrada de Dados
		escreva("\nQuanto dinheiro você tem? R$")
		leia(money)

		logico formato_24 = falso
		hora = c.hora_atual(formato_24)

		//Condição
		se ((money >= 20) e (hora == 12)) 
		{
			escreva("Agora são " + hora + " horas. Você consegue comprar seu ingresso!")
		}
		senao
		{
			escreva("Agora são " + hora + " horas. Infelizmente não é possivel comprar o ingresso!")
		}

		escreva("\n\n")

		/* Solução do professor:
		 *  
		 * 	inteiro h = 15 
		 * 	real p = 20.0
		 *   
		 * 	escreva("")
		 * 
		 */
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1123; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */