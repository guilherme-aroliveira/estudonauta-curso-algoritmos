programa
{
	/* Ex 023 - Programa que lê o ano e 
	 *  exibe uma mensagem
	 *  a pessoa não pode comprar.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */

	inclua biblioteca Calendario --> c
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 023 - Serviço Militar v2.0 }")

		// Declaração de Variáveis
		inteiro anoNasc, idade

		// Entrada de Dados
		escreva("\n\nEm que ano você nasceu? ")
		leia(anoNasc)
		idade = c.ano_atual() - anoNasc
		escreva("Você tem " + idade + " ano(s)")
		escreva ("\n----------------------------------------")

	
		// Cálculos
		inteiro idadeRest = 18 - idade
		inteiro anoRest = c.ano_atual() + idadeRest
		inteiro idadeF = idade - 18
		inteiro anoF =  anoNasc + 18      

		// Condição e Saída de Dados
		se (idade == 18)
		{
			escreva("\nVocê completa 18 anos nesse ano de " + c.ano_atual())
		}
		senao
			se (idade < 18)
			{
				escreva("\nVocê ainda não completou 18 anos. Vai acontecer em " + anoRest)
				escreva("\nAinda falta " + idadeRest + " ano(s)")
			}
			senao
			{
				escreva("\nVocê já deveria ter se alistado em " + anoF)
				escreva("\nVocê está atrasado em " + idadeF + " ano(s)")
			}

		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 708; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */