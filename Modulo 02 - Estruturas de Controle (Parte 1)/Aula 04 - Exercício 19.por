programa
{
	/* Ex 019 - Programa que lê um numero, caso ele seja positivo
	 *  exibe o inverso dele, se for negativo exibe o oposto
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	 
	inclua biblioteca Matematica --> m
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 019 - Inverso ou Oposto }")

		// Declaração das Variáveis
		real num, inverso, oposto

		escreva("\n\n====== Números positivos: INVERSO | Outros: POSITIVO ======")

		// Entrada de Dados
		escreva("\nDigite um número: ")
		leia(num)

		escreva("-----------------------------------------------------------")

		// Cálculos
		inverso = 1 / num
		oposto = m.valor_absoluto(num)
		// oposto = num * (-1)

		// Condição e Saída de Resultados
		se (num >= 0)
		{
			escreva("\nO inverso de ", num, " é igual a ", inverso)
		}
		senao 
		{
			escreva("\nO oposto de ", num, " é igual a ", oposto)	
		}

		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 680; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */