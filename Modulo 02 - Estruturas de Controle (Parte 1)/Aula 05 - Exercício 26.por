programa
{
	/* Programa que exibe um menu de opções e realiza 
	 *  operações aritméticas de acordo com a opção 
	 *  escolhida pelo usuário.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	inclua biblioteca Tipos --> t
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 026 - Super Tabuada v1.0 }")

		// Declaração das Variáveis
		inteiro num1, num2 
		caracter opcao 

		// Menu de Opções
		escreva("\n\n\t=========================")
		escreva("\n\t+ \tAdição")
		escreva("\n\t- \tSubtração")
		escreva("\n\t* \tMultiplicação")
		escreva("\n\t/ \tDivisão")
		escreva("\n\t=========================")

		// Entrada de Dados
		escreva("\n\tDigite sua opção => ")
		leia(opcao)
		escreva("\tVocê escolheu a operação [" + opcao + "]" )

		// Validando Operador
		escolha (opcao)
		{
			caso '1': 
				opcao = '+'
				pare
			caso '2':
				opcao = '-'
				pare
			caso '3':
				opcao = '*'
				pare
			caso '4':
				opcao = '/'
				pare
			caso contrario:
				opcao = '+'
				pare				
		}

		escreva("\n\nDigite o primeiro número: ")
		leia(num1)
		escreva("Digite o segundo número: ")
		leia(num2)
		escreva("-----------------------------------------")

		escreva("\nCalculando valor de: " + num1 + " " + opcao + " " + num2)
		escreva("\nResultado da operação de ")

		// Escolha-Caso
		escolha (opcao)
		{
			caso '+':
				escreva("SOMA é = " + (num1 + num2))
				pare
			caso '-':
				escreva("SUBTRAÇÃO é = " + (num1 - num2))
				pare
			caso '*':
				escreva("MULTIPLICAÇÃO é = " + (num1 * num2))
				pare
			caso '/':
				escreva("DIVISÃO é = " + (t.inteiro_para_real(num1) / num2))
				pare
			caso contrario:
				escreva("Não foi possível fazer tal operação. Tente novamente.")
				pare			 	     
		}

		escreva("\n-----------------------------------------")
		escreva("\n\tVOLTE SEMPRE")

		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 770; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */