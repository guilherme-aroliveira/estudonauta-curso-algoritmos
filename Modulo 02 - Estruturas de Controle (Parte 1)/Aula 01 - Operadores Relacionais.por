programa
{
	
	/* Aula 02 - Operadores Relacionais
	 * 
	 */
	
	funcao inicio()
	{
		//
		inteiro a = 7, b = 9

		//
		escreva(a==b) // uma expressão com operador relacional o resultado sempre será um valor lógico
		escreva("\n")
		escreva(a == 8)
		escreva("\n")
		escreva(b == 9)
		escreva("\n")
		escreva(a != b)

		// não se pode juntar operadores relacionais numa relação simples				   

		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 15; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */