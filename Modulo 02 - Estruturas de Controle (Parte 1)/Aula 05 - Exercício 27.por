programa
{
	inclua biblioteca Matematica --> m
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 027 -  Seu peso nos planetas }")

		// Declaração de Variáveis
		real peso, massa, a
		inteiro opcao 

		// Entrada de Dados 
		escreva("\n\nQual é o seu peso aqui na Terra (Kg): ")
		leia(massa)

		// Menu de opções
		escreva("\n\t   ESCOLHA UM PLANETA    ")
		escreva("\n\t=======================")
		escreva("\n\t1\tMercúrio")
		escreva("\n\t2\tVênus")
		escreva("\n\t3\tMarte")
		escreva("\n\t4\tJúpiter")
		escreva("\n\t5\tSaturno")
		escreva("\n\t6\tUrano")
		escreva("\n\t=======================")
		escreva("\n\tDigite sua opção => ")
		leia(opcao)

		escreva("\n-------------------------------------------")
		escreva("\n")
		
		// Calculo do Peso
		escolha (opcao)
		{
			caso 1: //Mercúrio
				// peso = massa x gravidade
				peso = massa * 0.38 
				a = m.arredondar(peso, 2)
				escreva("No planeta MERCÚRIO, seu peso seria " + a + "Kg")
				pare
			caso	2:
				peso = massa * 0.91 
				a = m.arredondar(peso, 2)
				escreva("No planeta VÊNUS, seu peso seria " + a + "Kg")
				pare
			caso 3:
				peso = massa * 0.38 
				a = m.arredondar(peso, 2)
				escreva("No planeta MARTE, seu peso seria " + a + "Kg")
				pare
			caso 4:
				peso = massa * 2.34 
				a = m.arredondar(peso, 2)
				escreva("No planeta JÚPITER, seu peso seria " + a + "Kg")
				pare
			caso 5: 
				peso = massa * 1.06 
				a = m.arredondar(peso, 2)
				escreva("No planeta SATURNO, seu peso seria " + a + "Kg")
				pare
			caso 6:
				peso = massa * 0.92 
				a = m.arredondar(peso, 2)
				escreva("No planeta URANO, seu peso seria " + a + "Kg")
				pare
			caso contrario: // Digitou errado
				peso = 0
				escreva("Seu peso não pode ser calculado para \noutros planetas. Tente novamente!")
				pare					
		}

		escreva("\n-------------------------------------------")
		escreva("\n\tVOLTE SEMPRE!")

		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1734; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */