programa
{
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 028 - O preço por época }")

		// Declaração de Variáveis 
		real preco, novoP, porcen
		inteiro opcao

		// Entrada de Dados
		escreva("\n\nDigite o preço de um produto R$ ")
		leia(preco)

		// Menu de opções
		escreva("\n\t   ESCOLHA UM PERÍODO ")
		escreva("\n\t=======================")
		escreva("\n\t1\tCarnaval [+10%]")
		escreva("\n\t2\tFérias Escolares [+20%]")
		escreva("\n\t3\tDias das Crianças [+5%]")
		escreva("\n\t4\tBlack Friday [-30%]")
		escreva("\n\t5\tNatal [-5%]")
		escreva("\n\t=======================")
		escreva("\n\tDigite sua opção => ")
		leia(opcao)

		escreva("\n-----------------------------------")

		// Saída de Resultados
		escolha(opcao)
		{
			caso 1: // Carnaval
				// novoP = preco + (preco * 10 / 100)
				porcen = (preco * 10) / 100
				novoP = preco + porcen
				escreva("\nNa época de CARNAVAL, o preço do produto vai para R$" + novoP)
				pare	
			caso 2: // Ferias Escolares
				porcen = (preco * 20) / 100
				novoP = preco + porcen
				escreva("\nNa época de FÉRIAS ESCOLARES, o preço do produto vai para R$" + novoP)
				pare
			caso 3: // Dias das Crianças
				porcen = (preco * 5) / 100
				novoP = preco + porcen
				escreva("\nNa época do DIAS DAS CRIANÇAS, o preço do produto vai para R$" + novoP)
				pare
			caso 4: // Black Friday 
				porcen = (preco * 30) / 100
				novoP = preco - porcen
				escreva("\nNa época da BLACK FRIDAY, o preço do produto vai para R$" + novoP)
				pare			
			caso 5: // Natal
				porcen = (preco * 5) / 100
				novoP = preco - porcen
				escreva("\nNa época do NATAL, o preço do produto cai para R$" + novoP)
				pare	
			caso contrario:
				escreva("\nOpção inválida. Tente Novamente!")
				pare	
		}

		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 800; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */