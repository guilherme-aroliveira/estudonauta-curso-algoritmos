programa
{
	/* Ex 024 - Programa que lê o Estado em  
	 *  que uma pessoa nasceu e exibe uma mensagem
	 *  a pessoa não pode comprar.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	 
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 024 - Qual é o seu estado? }")

		// Declaração de Variáveis
		cadeia natuEst, estado

		// Entrada de Dados
		escreva("\n\nEm que estado do Brasil você nasceu? ")
		leia(natuEst) 

		// COndição
		se (natuEst == "sp" ou natuEst == "SP")
		{
			escreva("Nascendo no " + natuEst + " você é PAULISTA")
		}
		senao se (natuEst == "mg" ou natuEst == "MG")
		{
			escreva("Nascendo no " + natuEst + " você é MINEIRO")
		}
		senao se (natuEst == "rj" ou natuEst == "RJ")
		{
			escreva("Nascendo no " + natuEst + " você é FLUMINENSE")
		}
		senao se (natuEst == "sc" ou natuEst == "SC")
		{
			escreva("Nascendo no " + natuEst + " você é BARRIGA VERDE")
		}
		senao se (natuEst == "rn" ou natuEst == "RN")
		{
			escreva("Nascendo no " + natuEst + " você é POTIGUAR")
		}
		senao
		{
			escreva("Nascendo no " + natuEst + " você é natural de sua cidade, mas não sei como te chamar!")	
		}

		escreva("\n")

		/* Resolução do Professor
		 *  inclua biblioteca Texto --> t
		 *  
		 * estado = t.caixa_alta(natuEst) 
		 * escreva("Nascendo no Estado " + estado + voce é: )
		 * 
		 * se (natuEst == "rj")
		 * {
		 * 	escreva("CARIOCA")
		 * }
		 * 
		 */
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1385; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */