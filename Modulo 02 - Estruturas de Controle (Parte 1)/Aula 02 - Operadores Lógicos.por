programa
{
	
	funcao inicio()
	{
		logico r = nao (5 > 3) ou (10 >= 10) e (7 < 4)
		escreva(r)

		escreva("\n")

		inteiro a = 3, b = 5
		logico x = (a > b) ou (b > a*2) e nao (b < a)
		/* 
		 *  logico x = (falso) ou (verdadeira) e nao (verdadeiro)
		 *  logico x =	(falso) ou (verdadeiro) e falso
		 *  logico x =	falso ou falso 
		 *  logico x =	verdadeiro
		 *  
		 */
		escreva(x) 

		escreva("\n\n")

	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 412; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */