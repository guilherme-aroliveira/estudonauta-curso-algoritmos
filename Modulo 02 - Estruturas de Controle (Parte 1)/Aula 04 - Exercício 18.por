programa
{
	
	/* Ex 018 - Programa que lê a distância e calcula o preço 
	 *  da passagem. Caso seja maior eu 200, o valor aumenta.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */

	 inclua biblioteca Matematica --> m
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 018 - Preço da Passagem }")

		// Declaração das Variáveis
		real dist, precoF, valor

		
		escreva("\n\n            VIAÇÃO ESTUDONAUTA              ")
		escreva("\n---------------------------------------------------")
		escreva("\nVIAGENS ATÉ 200km:\t\t\tR$0,50/km")
		escreva("\nVIAGENS ACIMA DE 200km:\t\t\tR$0,35/km")
		escreva("\n===================================================")

		//Entrada dos Dados
		escreva("\nInforme a distância total da viagem, em Km: ")
		leia(dist)

		// Condição
		se (dist <= 200)
		{
			valor = 0.5
		}
		senao
		{
			valor = 0.35
		}

		// Cálculo
		precoF = dist * valor
		
		escreva("\n---------------- RESULTADO --------------------")
		escreva("\nUma viagem de " + dist + "Km vair custar R$" + m.arredondar(valor, 2) + "/km.")
		escreva("\nValor total: R$" + m.arredondar(precoF, 2)) 
		escreva("\n===============================================")
		
		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1037; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */