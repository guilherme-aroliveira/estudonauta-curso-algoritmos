programa
{
	/* Ex 025 - Programa que lê três valores e 
	 *  coloca em ordem
	 *  a pessoa não pode comprar.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 025 - Três valores em ordem }")

		// Declaração de Variáveis
		inteiro num1, num2, num3

		// Entrada de Dados
		escreva("\n\nDigite um valor: ")
		leia(num1)
		escreva("Digite outro valor: ")
		leia(num2)
		escreva("Digite mais um valor: ")
		leia(num3)
		escreva("-------------------------------")

		// Condição
		se (num1 > num2 e num2 > num3)
		{
			escreva("\nMAIOR: " + num1 + "\nINTERMEDIÁRIO: " + num2 + "\nMENOR: " + num3)
		}
		senao se (num2 > num1 e num1 > num3)
			{
			 	escreva("\nMAIOR: " + num2 + "\nINTERMEDIÁRIO: " + num1 + "\nMENOR: " + num3)
			}
			senao se(num3 > num2 e num2 > num1) 
			{
				escreva("\nMAIOR: " + num3 + "\nINTERMEDIÁRIO: " + num2 + "\nMENOR: " + num1)
			}
			senao
			{
				escreva("\nMAIOR: " + num3 + "\nINTERMEDIÁRIO: " + num1 + "\nMENOR: " + num2)
			}

		escreva("\n")

		/* Resolução do professor: 
		 * 
		 */
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1068; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */