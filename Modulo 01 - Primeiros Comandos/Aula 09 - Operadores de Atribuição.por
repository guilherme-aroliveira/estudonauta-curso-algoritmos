programa
{
	
	funcao inicio()
	{
		inteiro n = 6
		n+=2  // n = n + 2
		escreva(n)

		inteiro n1 = 10
		n1 /= 2 // n = n / 2
		escreva(n1)

		inteiro n2 = 10
		n2 ++
		escreva(n2)

	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 185; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */