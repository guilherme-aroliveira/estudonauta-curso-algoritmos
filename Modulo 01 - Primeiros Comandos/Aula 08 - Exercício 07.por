programa
{
	/* Ex 007 - Progrma para ler a largura e altura de uma parede retangular.
	 * O programa vai calcular a área da parede, além da quantidade de tinta 
	 * necessária para pintá-la, sabendo que cada litro de tinta pinta um m² de parede.  
	 * Autor: Guilherme Oliveira  
	 * Instituição: Estudonauta
	 */
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 007 - Pintando uma parede } \n")
		escreva("\nInformação importante: 1 litro de tinta pinta 2m² de parede \n")
		escreva("--------------------------------------------------- \n")

		// Declaração de Variáveis
		real l, a

		// Entrada de dados
		escreva("Largura da parede: (m) ")
		leia(l)
		escreva("Altura da parede: (m) ")
		leia(a)

		// Calculos
		real area = l * a
		real latasT = area / 2

		// Saída dos Resultados
		escreva("--------------------------------------------------- \n")
		escreva("Uma parede " + l + " x " + a + " tem uma área de " + area + "m² \n")
		escreva("Precisaremos de " + latasT + " litros de tinta.")

		escreva("\n\n")
		
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 245; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */