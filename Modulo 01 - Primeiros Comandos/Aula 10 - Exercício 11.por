programa
{
	/* Ex 011 - Programa que leia a cidade onde a pessoa mora, 
	 *  mostre o nome da cidade em caixa alta, qual é a primeira 
	 *  letra do nome da cidade e quantos caracteres ela tem.
	 * Autor: Guilherme Oliveira 
	 * Instituição: Estudonauta
	 */
	 
	inclua biblioteca Texto --> txt
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 011 - Analisando sua cidade } \n")

		// Declaração de Variáveis
		cadeia cidade
		
		// Entrada de Dados
		escreva("\nEm qual cidade você mora? ")
		leia(cidade)

		// Cáculos
		cadeia grande = txt.caixa_alta(cidade)
		inteiro tam = txt.numero_caracteres(cidade)

		escreva("\n")

		// Saída de Dados
		escreva("--------- ANALISANDO --------- \n")
		escreva("Você mora na cidade " + grande + "\n")
		escreva("A primeira letra é " + txt.obter_caracter(cidade, 0) + "\n")
		escreva("E contém " + tam + " caracteres." + "\n")

		

		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 595; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */