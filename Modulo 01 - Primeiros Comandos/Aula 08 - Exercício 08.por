programa
{
	/* Ex 008 - Programa para ler o preço de um produto 
	 *  e aplicar 5% de desconto em cima do valor informado
	 * Autor: Guilherme Oliveira 
	 * Instituição: Estudonauta
	 */
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 008 - Desconto no produto } \n")

		// Declaração de Variáveis
		real precoP
		
		// Entrada de Dados
		escreva("\nQual é o preço do produto? R$")
		leia(precoP)

		// Cálculos
		real desc = (precoP * 5) / 100
		real precoF = precoP - desc

		// Saída de Resultados
		escreva("------------------------------------------")
		escreva("\nO produto no valor de R$" + precoP)
		escreva("\nCom 5% de desconto, o produto sai por: R$" + precoF + "\n")
		escreva("O cliente teve uma economia de R$" + desc)

		escreva("\n\n")
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 121; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */