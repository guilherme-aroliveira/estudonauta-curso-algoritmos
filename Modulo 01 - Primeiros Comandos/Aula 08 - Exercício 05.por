programa
{
	/*
	 * Ex 005 - Programa para ler duas notas de um aluno e calcular a média
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 005 - Média do aluno } \n")

		// Declaração de Variáveis
		real nota1, nota2, media

		//Entrada de Dados
		escreva("\nPrimeira nota: ")
		leia(nota1)
		escreva("Segunda nota: ")
		leia(nota2)

		escreva("\n")

		media = (nota1 + nota2) / 2

		//Saida de Resultados
		escreva("------------RESULTADO ------------\n")
		escreva("As notas do aluno foram " + nota1 + " e " + nota2 + "\n")
		escreva("A média final foi " + media)

		escreva("\n\n")

	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 639; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */