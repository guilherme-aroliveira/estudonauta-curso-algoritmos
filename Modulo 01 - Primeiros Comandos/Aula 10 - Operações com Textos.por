programa
{
	inclua biblioteca Texto --> txt
	
	funcao inicio()
	{
		cadeia nome = "Estudonauta"
		escreva(txt.caixa_alta(nome))
		escreva("\n")
		escreva(txt.obter_caracter(nome, 5))
		escreva("\n")
		escreva(txt.extrair_subtexto(nome, 0, 3))
		escreva("\n")
		escreva(txt.posicao_texto("a", nome, 0))
		escreva("\n")
		escreva(txt.substituir(nome, "d", "X"))
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 347; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */