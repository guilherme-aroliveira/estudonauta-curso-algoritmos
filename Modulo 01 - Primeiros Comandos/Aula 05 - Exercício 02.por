programa
{
	/*
	 * Ex 002 - Prgograma para ler o nome, ano de nascimento e salário,
	 * mostrando em seguida um ficha funcional.
	 * Autor: Guilherme Oliveira
	 * Instituição de Ensino: Estudonauta
	 */
	
	funcao inicio()
	{
		// Declaração das Variavéis
		cadeia nome
		inteiro ano
		real salario

		// Entrada de Dados
		escreva("\nNome do Funcionário: ")
		leia(nome)
		escreva("Ano de nascimento: ")
		leia(ano)
		escreva("Salário R$: ")
		leia(salario)

		escreva("\n")
		
		// Saída dos Resultados
		escreva("\n--------- FICHA FUNCIONAL ---------- \n")
		escreva("NOME: " + nome + "\n")
		escreva("NASCIMENTO em " + ano + "\n")
		escreva("SALÁRIO de " + salario + "\n")
		escreva("------------------------------------ \n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 58; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */