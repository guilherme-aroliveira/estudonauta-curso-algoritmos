programa
{
	/* Ex 012 - Programa que leia o nome completo de uma pessoa
	 *  e mostre apenas o primeiro nome.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	 
	inclua biblioteca Texto --> txt
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 012 - Seu nome } \n")

		// Declaração de Variáveis
		cadeia nome
		
		// Entrada de Dados
		escreva("\nDigite seu nome completo: ")
		leia(nome)

		escreva("\n")

		inteiro pos = txt.posicao_texto(" ", nome, 0)
		cadeia pnome = txt.extrair_subtexto(nome, 0, pos)
		cadeia grande = txt.caixa_alta(pnome)

		// Saída dos resultados
		escreva("------------ ANALISANDO -------------")
		escreva("\nSeu primeiro nome é: " + grande)

		escreva("\n\n")

	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 682; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */