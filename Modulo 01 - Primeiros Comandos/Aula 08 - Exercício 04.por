programa
{
	/*
	 * Ex 004 - Programa que lê um númeor inteiro qualquer e 
	 * mostra as operações: soma, diferença, produto, quoficente real, quociente inteiro e módulo.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	 
	inclua biblioteca Tipos --> t
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 004 - Operações Aritméticas } \n")
		
		//Declaração de Variáveis
		inteiro n1, n2

		// Entrada de Dados
		escreva("\nDigite um valor: ")
		leia(n1)
		escreva("Digite outro valor: ")
		leia(n2)

		escreva("\n")
		
		// Resultados Finais
		escreva("---------- RESULTADOS -------------- \n")
		escreva("SOMA = " + (n1 + n2) + "\n")
		escreva("DIFERENÇA = " + (n1 - n2) + "\n")
		escreva("DIVISÃO INTEIRA = " + (n1 / n2) + "\n")
		escreva("DIVISÃO REAL = " + (t.inteiro_para_real(n1) / t.inteiro_para_real(n2)) + "\n")
		escreva("RESTO DA DIVISÃO = " + (n1 % n2))

		escreva("\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 820; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */