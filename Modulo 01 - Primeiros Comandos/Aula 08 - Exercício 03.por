programa
{
	/*
	 * Ex 003 - Programa para ler um número inteiro qualquer e mostrar seu antecessor e sucessor.
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	funcao inicio()
	{
		// Declaração de Variáveis
		inteiro num

		// Entrada de Dados
		escreva("\nMe diga um número: ")
		leia(num)
		escreva("---------------------------------------")

		escreva("\n")

		// Resultados Finais
		escreva("O antecessor de " + num + " é o valor " + (num - 1) + "\n")
		escreva("O sucessor " + num + " é o valor " + (num + 1))
		escreva("\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 359; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */