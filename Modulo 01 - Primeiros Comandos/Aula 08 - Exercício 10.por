programa
{
	/* Ex 010 - Programa que calcule quantos dias de vida um fumante já perdeu.
	 *  A base do cáculo vem de uam pesquisa que diz que cada cigarro consumido
	 *  reduz o tempo de vida do fumante em 10 min.
	 * Autor: Guilherme Oliveira  
	 * Instituição: Estudonauta 
	 */
	 
	inclua biblioteca Tipos --> t
	inclua biblioteca Matematica --> m
	 
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 010 - Não fume } \n")
		
		escreva("\nDados da OMS: Cada cigarro reduz 10 minutos de vida \n")
		escreva("------------------------------------- \n")

		// Declração de Variáveis
		inteiro anos, cigars 

		// Entrada de Dados
		escreva("Há quantos anos você fuma? ")
		leia(anos)
		escreva("Quantos cigarros você fuma por dia? ")
		leia(cigars)

		// Cálculos
		inteiro totCigars = cigars * 365 * anos // Cada ano tem 365 dias
		real dias = (t.inteiro_para_real(totCigars) * 10) / (24 * 60) // Cada dia tem 1140 minutos

		// Saída dos Resultados	
		escreva("\n---------------- RESULTADO --------------------- ")
		escreva("\nAo todo, até agora você já fumou " + totCigars + " cigarros!")
		escreva("\nEstima-se que você já perdeu " + m.arredondar(dias,2) + " dias de vida.")

		escreva("\n\n")
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 213; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */