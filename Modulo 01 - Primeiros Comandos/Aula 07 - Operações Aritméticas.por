programa
{
	inclua biblioteca Matematica --> m // apelido para a biblioteca 
	inclua biblioteca Tipos --> t
	
	funcao inicio()
	{
		// Cálculo de porcentagem
		real preco = 1500.22
		
		escreva("Resultado = " + (preco - ((preco*30) / 100)))
		escreva("\n")

		// Ordem de precedência
		escreva("Resultado = " + (4 + 2 / 2) + "\n")
		escreva("Resultado = " + ((4 + 2) / 2) + "\n")

		// Cálculo de média de notas
		real n1, n2
		
		escreva("Informe a primeira nota: ")
		leia(n1)
		escreva("Informe a segunda nota: ")
		leia(n2)
		
		real m = (n1 + n2) / 2
		escreva("A média é: " + m + "\n")

		// Cálculo de desconto 
		real preco1, npreco
		escreva("Preço do produto: R$ ")
		leia(preco1)
		npreco = preco1 - (preco1 * 30 / 100)
		escreva("O novo preço é de: R$ " + npreco + "\n")

		//Cálculo de raiz
		inteiro n = 9
		real raiz = m.raiz(n,2)
		escreva("A Raiz do número é: " + raiz + "\n")

		//Divisão com numeros inteiros com resultado em numero real
		inteiro a = 7, b = 2
		real r = t.inteiro_para_real((a) / b)
		escreva(r)

		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 107; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */