programa
{
	/* Ex 006 - Programa para ler uma distância em METROS e 
	 *  convertê-la em todas as demais medidas de comprimento
	 * Autor: Guilherme Oliveira
	 * Instituição: Estudonauta
	 */
	 
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 006 - Conversor de medidas } \n")

		// Declaração de Variáveis
		real metro
		
		// Entradas de Dados
		escreva("\nDistância em metros: ")
		leia(metro)

		escreva("\n")

		// Saída dos Resultados
		escreva("--------- CONVERTENDO " + metro + " m ---------- \n")
		escreva("| " + (metro / 1000) + " Km\n")
		escreva("| " + (metro / 100) + " Hm\n")
		escreva("| " + (metro / 10) + " Dam\n")
		escreva("| " + (metro * 10) + " dm\n")
		escreva("| " + (metro * 100) + " cm\n")
		escreva("| " + (metro * 1000) + " mm\n")
		escreva("V\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 770; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */