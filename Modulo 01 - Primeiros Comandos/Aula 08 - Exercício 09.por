programa
{
	/* Ex 009 - Programa para ler o salário de um funcionário e aplicar 
	 *  um reajuste (aumento) personalizável ao valor digitado.
	 * Autor: Guilherme Oliveira  
	 * Instituição: Estudonauta 
	 */
	
	funcao inicio()
	{
		escreva("{ EXERCÍCIO 009 - Aumento Salarial } \n")

		// Declaração de Variáveis
		cadeia nome
		real salario, porcen

		// Entrada de Dados
		escreva("Nome do funcionário: ")
		leia(nome)
		escreva("Salário: R$")
		leia(salario)
		escreva("Reajuste (%): ")
		leia(porcen)

		escreva("\n")

		// Cálculos
		real valor = salario * porcen / 100
		real novoSal = salario + valor

		// Saída dos Resultados
		escreva("--------- RESULTADO ----------- \n")
		escreva("O funcionário " + nome + " ganhava R$" + salario + "\n")
		escreva("e depois de ganhar " + porcen + "% de aumento \n")
		escreva("vai passar a ganhar R$" + valor + " a mais por mês.")
		escreva("\nSeu novo salário será de R$" + novoSal)
		
		escreva("\n\n")
		

		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 203; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */